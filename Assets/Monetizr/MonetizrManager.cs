﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Monetizr.Challenges;
using UnityEngine;
using UnityEngine.Networking;

namespace Monetizr
{
    /// <summary>
    /// Predefined asset types for easier access
    /// </summary>
    public enum AssetsType
    {
        TitleString,
        BannerTexture2D,
        BannerSprite,
        IconTexture2D,
        IconSprite,
        MeshFilter,
    }

    /// <summary>
    /// Challenge extension for easier access to Challenge assets
    /// </summary>
    public class ChallengeExtension
    {
        private static readonly Dictionary<AssetsType, Type> AssetsSystemTypes = new Dictionary<AssetsType, Type>()
        {
            { AssetsType.TitleString, typeof(String) },
            { AssetsType.BannerTexture2D, typeof(Texture2D) },
            { AssetsType.IconTexture2D, typeof(Texture2D) },
            { AssetsType.BannerSprite, typeof(Sprite) },
            { AssetsType.IconSprite, typeof(Sprite) },
            { AssetsType.MeshFilter, typeof(MeshFilter) },

        };

        public Challenge Challenge { get; private set; }
        private readonly Dictionary<AssetsType, object> _assets = new Dictionary<AssetsType, object>();

        public ChallengeExtension(Challenge challenge)
        {
            this.Challenge = challenge;
        }

        public void SetAsset(AssetsType t, object asset)
        {
            _assets.Add(t, asset);
        }

        public T GetAsset<T>(AssetsType t)
        {
            if(AssetsSystemTypes[t] != typeof(T))
                throw new ArgumentException($"AssetsType {t} and {typeof(T)} do not match!");

            if(!_assets.ContainsKey(t))
                throw new ArgumentException($"Requested asset {t} doesn't exist in challenge!");

            return (T)Convert.ChangeType(_assets[t],typeof(T));
        }

    }

    /// <summary>
    /// Extension to support async/await in the DownloadAssetData
    /// </summary>
    public static class ExtensionMethods
    {
        public static TaskAwaiter GetAwaiter(this AsyncOperation asyncOp)
        {
            var tcs = new TaskCompletionSource<object>();
            asyncOp.completed += obj => { tcs.SetResult(null); };
            return ((Task)tcs.Task).GetAwaiter();
        }
    }

    static class DownloadHelper
    {
        /// <summary>
        /// Downloads any type of asset and returns its data as an array of bytes
        /// </summary>
        public static async Task<byte[]> DownloadAssetData(string url, Action onDownloadFailed = null)
        {
            UnityWebRequest uwr = UnityWebRequest.Get(url);

            await uwr.SendWebRequest();

            if (uwr.isNetworkError)
            {
                Debug.LogError(uwr.error);
                onDownloadFailed?.Invoke();
                return null;
            }

            return uwr.downloadHandler.data;
        }
    }

    /// <summary>
    /// Main manager for Monetizr
    /// </summary>
    public class MonetizrManager : MonoBehaviour
    {
        private ChallengesClient _challengesClient;
        private static MonetizrManager _instance;

        //Storing ids in separate list to get faster access (the same as Keys in challenges dictionary below)
        private readonly List<string> _challengesId = new List<string>();
        private readonly Dictionary<string, ChallengeExtension> _challenges = new Dictionary<string, ChallengeExtension>();

        public static MonetizrManager Initialize(string apiKey, Action onRequestComplete)
        {
            Debug.Log("MonetizrManager Initialize");

            if (_instance != null)
            {
                _instance.RequestChallenges(onRequestComplete);
                return _instance;
            }

            var monetizrObject = new GameObject("MonetizrManager");
            var monetizrManager = monetizrObject.AddComponent<MonetizrManager>();

            DontDestroyOnLoad(monetizrObject);
            _instance = monetizrManager;

            monetizrManager._Initialize(apiKey, onRequestComplete);

            return _instance;
        }
  
        public static MonetizrManager Instance => _instance;

        /// <summary>
        /// Initialize
        /// </summary>
        private void _Initialize(string apiKey, Action onRequestComplete)
        {
            _challengesClient = new ChallengesClient(apiKey)
            {
                playerInfo = new PlayerInfo("Helsinki", 18, "action", "monetizr_mj")
            };

            RequestChallenges(onRequestComplete);
        }

        /// <summary>
        /// Helper function to download and assign graphics assets
        /// </summary>
        private static async Task AssignAssetTextures(ChallengeExtension ech, Challenge.Asset asset, AssetsType texture, AssetsType sprite)
        {
            var data = await DownloadHelper.DownloadAssetData(asset.url);

            var tex = new Texture2D(0, 0);
            tex.LoadImage(data);

            Sprite s = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

            ech.SetAsset(texture, tex);
            ech.SetAsset(sprite, s);
        }

        /// <summary>
        /// Request challenges from the server
        /// </summary>
        private async void RequestChallenges(Action onRequestComplete)
        {
            var list = await _challengesClient.GetList();

            foreach(var ch in list)
            {
                var ech = new ChallengeExtension(ch);

                if (this._challenges.ContainsKey(ch.id))
                    continue;

                await AssignAssetTextures(ech, ch.assets.Find(asset => asset.type == "banner"),
                    AssetsType.BannerTexture2D, AssetsType.BannerSprite);

                await AssignAssetTextures(ech, ch.assets.Find(asset => asset.type == "icon"),
                    AssetsType.IconTexture2D, AssetsType.IconSprite);


                ech.SetAsset(AssetsType.TitleString, ch.title);

                this._challenges.Add(ch.id, ech);
                _challengesId.Add(ch.id);
            }

            Debug.Log("RequestChallenges completed with count: " + list.Count);

            onRequestComplete?.Invoke();
        }

        /// <summary>
        /// Get list of the available challenges
        /// </summary>
        /// <returns></returns>
        public List<string> AvailableChallenges()
        {
            return _challengesId;
        }

        /// <summary>
        /// Get Asset from the challenge
        /// </summary>
        public T GetAsset<T>(string challengeId, AssetsType t)
        {
            return _challenges[challengeId].GetAsset<T>(t);
        }

        /// <summary>
        /// Single update for reward and claim
        /// </summary>
        public async void UpdateReward(string challengeId, int progress)
        {
            var challenge = _challenges[challengeId].Challenge;

            try
            {
                if (progress < 100)
                {
                    await _challengesClient.UpdateStatus(challenge, progress);
                }
                else
                {
                    await _challengesClient.Claim(challenge);
                }
            }
            catch (Exception e)
            {
                Debug.Log($"An error occured: {e.Message}");
            }
        }

    }
}