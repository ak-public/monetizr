﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] GameObject coinExplosion;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SoundManager.Instance.Coin();
            GameManager.Instance.wallet.Amount++;

            Instantiate(coinExplosion, transform.position, Quaternion.Euler(-90, 0, 0));
            Destroy(gameObject);
        }
    }
}
