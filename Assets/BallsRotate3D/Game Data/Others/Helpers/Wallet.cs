﻿using UnityEngine;

public class Wallet
{
    public delegate void OnCurrencyAmountChangeAction(int currentAmount);
    public OnCurrencyAmountChangeAction OnCurrencyAmountChange;

    string currencyName = "Currency";
    int currencyAmount = 0;

    public Wallet(string name, int startValue)
    {
        currencyName = name;
        Amount = startValue;
    }

    public Wallet(string name)
    {
        currencyName = name;
        Amount = PlayerPrefs.GetInt(currencyName, 1);
    }

    public Wallet()
    {
        currencyName = "Currency";
        Amount = PlayerPrefs.GetInt(currencyName, 1);
    }

    public int Amount
    {
        get
        {
            return currencyAmount;
        }
        set
        {
            currencyAmount = value;
            OnCurrencyAmountChange?.Invoke(value);
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt(currencyName, Amount);
    }
}
