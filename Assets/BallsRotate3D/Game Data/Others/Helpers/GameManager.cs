﻿//9b6b9bdc-eea8-4f17-b0c5-9a0fddd452b3

using Facebook.Unity;
using System;
using System.Collections.Generic;
using Monetizr;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public delegate void OnGameOverAction(bool playerWon);
    public static OnGameOverAction OnGameOver;

    public MissionsManager missionsManager;

    public GameObject loadingScreen;

    public int level = 1;

    public Wallet wallet;
    bool isGameOver = false;
    UIManager UIManager;

    int consoleAmount = 0;
    int resetAmount = 0;

    public ConsoleManager console = null;
    public AnalyticsManager analytics = null;
    //!!public MicroTasksManager microtasks = null;
    //public MonetizrManager monetizrManager = null;

    Player player;
       
    protected override void Awake()
    {
        base.Awake();

        console = ConsoleManager.Initialize(false);

        analytics = AnalyticsManager.Initialize();

        bool disableMT = false;// analytics.GetSegmentId() == 1;


#if UNITY_EDITOR_WIN
        disableMT = true;
#endif

        //PUHzF8UQLXJUuaW0vX0D0lTAFlWU2G0J2NaN2SHk6AA

        MonetizrManager.Initialize("PUHzF8UQLXJUuaW0vX0D0lTAFlWU2G0J2NaN2SHk6AA",
            () =>
            {
                missionsManager.Initialize();
                loadingScreen.SetActive(false);
            });

        Debug.Log("GameManager: Awake");
        //!!microtasks = MicroTasksManager.Initialize(disableMT);


        UIManager = FindObjectOfType<UIManager>();
        UIManager.UpdateProgress(0f);
        wallet = new Wallet("Coins");
        level = PlayerPrefs.GetInt("Level", 1);

        player = FindObjectOfType<Player>();
               
#if !UNITY_STANDALONE
        if(ServicesManager.instance != null)
        {
            ServicesManager.instance.InitializeUnityAds();
            ServicesManager.instance.InitializeAdmob();
        }
#endif

    }
        
    protected override void OnDestroy()
    {
        wallet.Save();
    }

    public void Reload()
    {
        SceneManager.LoadScene(0);
    }

    void PlayerWon()
    {
        analytics.LogEvent("player_won", "level", level.ToString());


        console.Print("[GameManager] PlayerWon");

        level++;
        PlayerPrefs.SetInt("Level", level);

        /*if (ServicesManager.instance != null)
        {
            ServicesManager.instance.ShowInterstitialAdmob();
            ServicesManager.instance.ShowInterstitialUnityAds();
        }*/

        //MicroTasks_Show(() => { HandleSkipLevel(true); });
    }

   
    void PlayerLost()
    {
        //analytics.LogEvent("player_lost", "level", level.ToString());

        /*if (microtasks == null || !microtasks.IsPossibleToShowAds())
        {
            analytics.LogEvent("player_lost_without_skip", "level", level.ToString());
        }
        else
        {
            analytics.LogEvent("player_lost_with_skip", "level", level.ToString());
        }*/

        console.Print("[GameManager] PlayerLost");

        //MicroTasks_Show(() => { HandleSkipLevel(true); });
    }

    internal void ToggleConsole()
    {
        Debug.Log(consoleAmount);

        consoleAmount++;

        if(consoleAmount > 10)
        {
            consoleAmount = 0;

            console.isEnabled = !console.isEnabled;
        }
    }

    internal void ToggleReset()
    {
        Debug.Log(consoleAmount);

        resetAmount++;

        if (resetAmount > 10)
        {
            resetAmount = 0;

            level = 0;
            PlayerPrefs.SetInt("Level", level);
        }
    }

    public void UpdateProgress(int x, int outOf)
    {
        UIManager.UpdateProgress((float)x / outOf);
    }

    internal void CollideWithObstacle()
    {
        UIManager.GameOver(false);
    }

    //only works now for win
    public void GameOver(bool playerWon)
    {
        if (isGameOver)
            return;

        isGameOver = true;

        UIManager.GameOver(playerWon);

        OnGameOver?.Invoke(playerWon);

        if (playerWon)
        {
            PlayerWon();
            SoundManager.Instance.Win();
        }
        else
        {
            PlayerLost();
            SoundManager.Instance.Lost();
        }
    }

    public void SkipLevel()
    {
        console.Print("[GameManager] SkipLevel");

        /*if (ServicesManager.instance != null)
        {
            ServicesManager.instance.ShowRewardedVideoAdAdmob();
            ServicesManager.instance.ShowRewardedVideoUnityAds();
        }*/

#if UNITY_EDITOR
        HandleSkipLevel(true);
#else
        HandleSkipLevel(true);
        //!!microtasks.Show(() => { HandleSkipLevel(true); }, () => { HandleSkipLevel(false); });
#endif

        // UIManager.HideRestartButton();

    }

    public void HandleSkipLevel(bool completed)
    {
        //continue
        if (completed)
        {
            console.Print("[GameManager] HandleSkipLevel");

            player.EnableMove();

            UIManager.HideLostPanel();
            //PlayerWon();
            //Reload();
        }
        else
        {
            Reload();
        }
    }
}
