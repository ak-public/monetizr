﻿using System.Collections.Generic;
using UnityEngine;

public class RoadFormingEffect : MonoBehaviour
{
    [SerializeField] float roadHeightDelta = 5.2f;
    [SerializeField] float boxRadius = 3f;
    [SerializeField] float sideRaius = 1f;
    [SerializeField] float containerRadius = 8f;
    [SerializeField] HashSet<BoxContainer> nearBoxContainers = new HashSet<BoxContainer>();

    BoxContainer[] sceneBoxContainers;

    private void Start()
    {
        sceneBoxContainers = FindObjectsOfType<BoxContainer>();       
    }

    private void Update()
    {
        for (int i = 0; i < sceneBoxContainers.Length; i++)
        {
            float dst = Magnitude(transform.position, sceneBoxContainers[i].transform.position);

            if (dst <= containerRadius)
            {
                nearBoxContainers.Add(sceneBoxContainers[i]);
            }
            else
            {
                nearBoxContainers.Remove(sceneBoxContainers[i]);
            }
        }

        foreach(BoxContainer boxContainer in nearBoxContainers)
        {
            for (int i = 0; i < boxContainer.cubes.Length; i++)
            {
                if (boxContainer.cubes[i].position.y >= 0)
                    continue;

                Vector3 originPos = boxContainer.cubes[i].position;
                float dst = Magnitude(transform.position, originPos);

                if (dst <= boxRadius)
                {
                    float dstNormilezed = dst / boxRadius;
                    float yPos = ((1 - dstNormilezed * dstNormilezed * dstNormilezed) * roadHeightDelta) - 5;
                    if (yPos > 0)
                        yPos = 0;

                    originPos.y = yPos;
                }
                else
                    originPos.y = -5.5f;

                boxContainer.cubes[i].position = originPos;
            }

            for (int i = 0; i < boxContainer.sidesCubes.Length; i++)
            {
                Vector3 originPos = boxContainer.sidesCubes[i].position;
                float dst = Magnitude(transform.position, originPos);

                if (dst <= sideRaius)
                {
                    float dstNormilezed = dst / sideRaius;
                    originPos.y = ((1 - dstNormilezed * dstNormilezed * dstNormilezed) * 5) - 5;
                }
                else
                    originPos.y = -5.5f;

                boxContainer.sidesCubes[i].position = originPos;
            }
        }
    }

    float Magnitude(Vector3 a, Vector3 b)
    {
        return (new Vector3(a.x, 0, a.z) - new Vector3(b.x, 0, b.z)).magnitude;
    }
}
