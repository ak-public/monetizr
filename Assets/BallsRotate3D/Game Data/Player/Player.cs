﻿using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float wheelSpeed = 10f;
    [SerializeField] Transform wheel;

    bool ableToMove = true;
    bool isMoving = false;
    bool playerKeptMoveButtonPressed = false;
    Vector3 movingDir;
    Transform pointA, pointB;
    Rigidbody rb;
    Animator anim;
    LevelGenerator levelGenerator;

    private void OnEnable()
    {
        //GameManager.OnGameOver += Finish;
    }

    private void OnDisable()
    {
        //GameManager.OnGameOver -= Finish;
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        levelGenerator = FindObjectOfType<LevelGenerator>();

        ableToMove = true;
        isMoving = false;
    }

    public void EnableMove()
    {
        enabled = true;
        ableToMove = true;
        isMoving = false;
    }

    private void Start()
    {
        pointA = levelGenerator.GetNextNode();
        pointB = levelGenerator.GetNextNode();

        transform.position = pointA.position;
    }

    private void Update()
    {
        if (isMoving)
            WheelControl();
    }

    private void FixedUpdate()
    {
        if (isMoving)
        {
            movingDir = pointB.position - pointA.position;
            movingDir.Normalize();

            rb.transform.forward = movingDir;
            rb.MovePosition(transform.position + movingDir * speed * Time.fixedDeltaTime);

            ChangeNodes();
        }
    }
       
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))
        {
            //GameManager.Instance.GameOver(false);

            var rb = other.gameObject.transform.parent.gameObject.GetComponentsInChildren<Collider>();

            Array.ForEach(rb, (c) => { c.enabled = false;  });

            //foreach (var b in rb)
            //    b.enabled = false;

            //IList a;
            //Array array;

            GameManager.Instance.CollideWithObstacle();

            StopMovement();
        }
    }

    void Finish(bool won)
    {
        if (won)
            anim.SetBool("Finish", true);
        else
            anim.SetTrigger("Stop");

        enabled = false;
    }

    void WheelControl()
    {
        wheel.Rotate(Vector3.right * wheelSpeed * Time.deltaTime);
    }

    void ChangeNodes()
    {
        if (Vector3.Distance(transform.position, pointB.position) <= 0.05f)
        {
            pointA = pointB;
            pointB = levelGenerator.GetNextNode();

            transform.position = pointA.position;
            
            StartCoroutine(Jump());
        }
    }

    IEnumerator Jump()
    {
        playerKeptMoveButtonPressed = true;
        StopMoving();
        ableToMove = false;

        yield return new WaitForSeconds(0.2f);

        pointA.GetComponent<Animator>().SetTrigger("Activate");
        anim.SetTrigger("Jump");

        yield return new WaitForSeconds(1f);
        ableToMove = true;

        if (playerKeptMoveButtonPressed)
            StartMoving();
    }

    void StartMoving()
    {
        GameManager.Instance.missionsManager.PlayerStartMove();

        isMoving = true;
        anim.SetTrigger("Start");
    }

    void StopMoving()
    {
        GameManager.Instance.missionsManager.PlayerEndMove();

        isMoving = false;
        anim.SetTrigger("Stop");
    }

    public void BeginMovement()
    {
        if (ableToMove)
            StartMoving();
        else
            playerKeptMoveButtonPressed = true;
    }

    public void StopMovement()
    {
        if (isMoving)
            StopMoving();

        playerKeptMoveButtonPressed = false;
    }
}
