﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    [SerializeField] Text prevText, nextText;
    [SerializeField] Text coinText;
    [SerializeField] Image progressImage;
    [SerializeField] Animator gameAnim;

    [SerializeField] GameObject sfxOff, sfxOn;
    [SerializeField] GameObject musicOff, musicOn;

    private void OnEnable()
    {
        GameManager.Instance.wallet.OnCurrencyAmountChange += UpdateCoins;
        //GameManager.OnGameOver += GameOver;
    }

    private void OnDisable()
    {
        GameManager.Instance.wallet.OnCurrencyAmountChange -= UpdateCoins;
        //GameManager.OnGameOver -= GameOver;
    }

    private void Start()
    {
        UpdateCoins(GameManager.Instance.wallet.Amount);

        prevText.text = GameManager.Instance.level.ToString();
        nextText.text = (GameManager.Instance.level + 1).ToString();

        SetSFX(SoundManager.Instance.SFX);
        SetMusic(SoundManager.Instance.Music);
    }

    void UpdateCoins(int coins)
    {
        coinText.text = coins.ToString();
    }

    public void HideRestartButton()
    {
        var restartButton = transform.Find("Game/Lost Panel/content/Next Button").gameObject;
        restartButton.SetActive(false);
    }

    public void HideLostPanel()
    {
        //gameAnim.ResetTrigger("Win");
        //gameAnim.ResetTrigger("Lost");
        //gameAnim.ResetTrigger("LostNoSkip");
        //gameAnim.SetTrigger("HideLost");

        
        gameAnim.Play("lostHide");
    }

    public void GameOver(bool playerWon)
    {
        GameManager.Instance.missionsManager.GamePlayEnd();


        if (playerWon)
        {
            gameAnim.SetTrigger("Win");
        }
        else
        {
            //!!MicroTasksManager mtm = GameManager.Instance.microtasks;

#if UNITY_EDITOR

            //gameAnim.SetTrigger(Random.Range(0,10000)%2 == 0 ? "Lost" : "LostNoSkip");

            gameAnim.SetTrigger("Lost");

#else
            //if no ads
            if (mtm == null || !mtm.IsPossibleToShowAds())
            {
                //HideRestartButton();
                gameAnim.SetTrigger("LostNoSkip");
            }
            else
            {
                gameAnim.SetTrigger("Lost");
            }
#endif



        }
    }

    public void UpdateProgress(float progress)
    {
        progressImage.fillAmount = progress;
    }

    public void ToggleSFX()
    {
        SoundManager.Instance.ToggleSFX();

        GameManager.Instance.ToggleReset();
    }

    public void ToggleMusic()
    {
        SoundManager.Instance.ToggleMusic();

        GameManager.Instance.ToggleConsole();
    }

    public void SetSFX(bool value)
    {
        sfxOff.SetActive(!value);
        sfxOn.SetActive(value);
    }

    public void SetMusic(bool value)
    {
        musicOff.SetActive(!value);
        musicOn.SetActive(value);
    }
    public void WatchAdsToUnlockSkin()
    {
 #if !UNITY_STANDALONE
        if (ServicesManager.instance != null)
        {
            ServicesManager.instance.ShowRewardedVideoUnityAds();
            ServicesManager.instance.ShowRewardedVideoAdAdmob();
        }
#endif
    }
}
