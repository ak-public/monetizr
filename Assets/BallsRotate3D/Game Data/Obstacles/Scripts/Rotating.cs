﻿using UnityEngine;

public class Rotating : MonoBehaviour
{
    [SerializeField] float speed = 5f;

    Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        rb.MoveRotation(Quaternion.Euler(0, transform.localEulerAngles.y + speed * Time.fixedDeltaTime, 0));
    }
}
