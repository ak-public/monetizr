﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinHolder : MonoBehaviour
{
    [SerializeField] Image iconImage;
    public SkinData data;

    public void UpdateSkin()
    {
        iconImage.gameObject.SetActive(data.IsUnlocked);
        iconImage.sprite = data.icon;
    }

    public void SetSkin(SkinData data)
    {
        this.data = data;
        UpdateSkin();
    }

    public void Select()
    {
        if (data.IsUnlocked)
        {
            data.Select();
            Shop.Instance.Select(this);
        }
    }
}
