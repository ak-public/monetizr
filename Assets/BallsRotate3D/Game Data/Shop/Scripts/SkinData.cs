﻿using UnityEngine;

[CreateAssetMenu(menuName = "Create Skin", fileName = "New Skin")]
public class SkinData : ScriptableObject
{
    public Sprite icon;
    public Texture2D texture;
    public bool isPreunlocked = false;

    public bool IsUnlocked
    {
        get
        {
            if (isPreunlocked)
                return true;

            return PlayerPrefs.GetInt(name + "IsUnlocked", 0) == 1 ? true : false;
        }
        set
        {
            if (value)
                PlayerPrefs.SetInt(name + "IsUnlocked", 1);
        }
    }

    public bool IsSelected()
    {
        return PlayerPrefs.GetString("SelectedSkin", "default") == name ? true : false;
    }

    public void Select()
    {
        PlayerPrefs.SetString("SelectedSkin", name);
    }
}
