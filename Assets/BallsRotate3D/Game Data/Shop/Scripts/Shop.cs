﻿using System.Collections.Generic;
using UnityEngine;

public class Shop : Singleton<Shop>
{
    [SerializeField] Material skinMaterial;
    [SerializeField] GameObject skinHolderPrefab;
    [SerializeField] Transform shopContent;
    [SerializeField] Transform selector;
    [SerializeField] SkinData[] skins;

    List<SkinHolder> skinholders = new List<SkinHolder>();

    protected override void Awake()
    {
        base.Awake();

        for (int i = 0; i < skins.Length; i++)
        {
            SkinHolder skinHolder = Instantiate(skinHolderPrefab, shopContent).GetComponent<SkinHolder>();
            skinHolder.SetSkin(skins[i]);

            if (skins[i].IsSelected())
            {
                Select(skinHolder);
            }

            skinholders.Add(skinHolder);
        }
    }

    public void Select(SkinHolder skinHolder)
    {
        selector.parent = skinHolder.transform;
        selector.localPosition = Vector3.zero;
        selector.localScale = Vector3.one;

        skinMaterial.SetTexture("_BaseMap", skinHolder.data.texture);
    }

    public void UnlockRandomSkin()
    {
        if (GameManager.Instance.wallet.Amount < 25)
            return;

        List<SkinData> lockedSkins = new List<SkinData>();

        for (int i = 0; i < skins.Length; i++)
        {
            if (!skins[i].IsUnlocked)
                lockedSkins.Add(skins[i]);

        }

        if (lockedSkins.Count == 0)
            return;

        GameManager.Instance.wallet.Amount -= 25;

        int r = Random.Range(0, lockedSkins.Count);
        lockedSkins[r].IsUnlocked = true;

        for (int i = 0; i < skinholders.Count; i++)
        {
            if (skinholders[i].data == lockedSkins[r])
            {
                skinholders[i].UpdateSkin();
            }
        }
    }
}
