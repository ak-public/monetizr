﻿using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField] ParticleSystem effect;

    private void OnEnable()
    {
        GameManager.OnGameOver += FinishEffect;
    }

    private void OnDisable()
    {
        GameManager.OnGameOver -= FinishEffect;
    }

    void FinishEffect(bool won)
    {
        if (won)
        {
            effect.Play();
        }
    }
}
