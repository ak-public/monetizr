﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadCubes : MonoBehaviour
{
    public float height = 2f;
    public float radius = 5f;
    [SerializeField] Transform target;
    [SerializeField] Transform[] cubes;

    private void Update()
    {
        foreach(Transform cube in cubes)
        {
            float dst = Vector3.Distance(new Vector3(target.position.x, 0, target.position.z), new Vector3(cube.position.x, 0, cube.position.z));

            if (dst < radius)
                cube.position = new Vector3(cube.position.x, (dst / radius) * height, cube.position.z);
            else
                cube.position = new Vector3(cube.position.x, height, cube.position.z);
        }
    }
}
