﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] int nodeCount = 3;
    [SerializeField] float distance = 5f;

    [Header("Prefabs")]
    [SerializeField] GameObject gradientPrefab; // 3.2f on X
    [SerializeField] GameObject nodePrefab;
    [SerializeField] GameObject finishPrefab;
    [SerializeField] GameObject boxContainer;
    [SerializeField] GameObject coinPrefab;

    [Header("Obstacles")]
    [Range(0, 100)]
    [SerializeField] int nodeObstacleProbability = 80;
    [SerializeField] GameObject[] nodeObsatcles;
    [Range(0, 100)]
    [SerializeField] int obstacleProbability = 50;
    [SerializeField] GameObject[] obstaclePrefabs;

    int levelSeed = 1;
    int lastNodeGiven = 0;
    List<Transform> nodes = new List<Transform>();
    List<Transform> obstacles = new List<Transform>();

    private void Awake()
    {
        levelSeed = PlayerPrefs.GetInt("Level", 1);
        SetDificulty();
        GenerateLevel();       
    }

    void SetDificulty()
    {
        if (levelSeed < 10)
        {
            nodeCount = levelSeed;
            if (nodeCount < 2)
                nodeCount = 2;

            nodeObstacleProbability = (int)((float)(levelSeed * 100) / 10);
            obstacleProbability = (int)((float)(levelSeed * 100) / 10);
        }
        else
        {
            nodeCount = 15;
            nodeObstacleProbability = 100;
            obstacleProbability = 80;
        }
    }

    void Clear()
    {
        for (int i = 0; i < nodes.Count; i++)
            Destroy(nodes[i].gameObject);

        nodes.Clear();

        for (int i = 0; i < obstacles.Count; i++)
            Destroy(obstacles[i].gameObject);

        obstacles.Clear();
    }

    void GenerateLevel()
    {
        lastNodeGiven = 0;
        Random.InitState(levelSeed);

        Vector3 previousPos = Vector3.zero;
        bool regenerate = false;

        for (int i = 0; i < nodeCount; i++)
        {
            string nodeName;
            Vector3 nodePosition = Vector3.zero;

            if (i == 0)
            {
                nodeName = "Start";
                nodePosition = Vector3.zero;
            }
            else 
            {
                bool generateObstacle = Random.Range(0, 100) < obstacleProbability ? true : false;

                if (i == nodeCount - 1)
                    nodeName = "Finish";
                else
                    nodeName = "Node" + (i + 1);

                int maxLoop = 500;
                while (true)
                {
                    nodePosition = RandomPointOnHexagon(previousPos, generateObstacle ? distance * 2 : distance);
                    bool isValid = true;

                    for (int j = 0; j < nodes.Count; j++)
                    {
                        float dst = Vector3.Distance(nodePosition, nodes[j].position);

                        if (dst < distance)
                            isValid = false;
                    }
                    maxLoop--;

                    if (isValid)
                        break;

                    if (maxLoop <= 0)
                    {
                        i = nodeCount + 1;
                        regenerate = true;

                        break;
                    }
                }

                if (generateObstacle)
                {
                    GenerateObstacle(previousPos, nodePosition);
                    if (i > 0)
                    {
                        Vector3 dir = (previousPos - nodePosition).normalized;
                        Instantiate(boxContainer, nodePosition + dir * distance * 1.5f, Quaternion.identity, transform).transform.right = dir;
                    }
                }
            }

            if (i > 0)
            {
                Vector3 dir = (previousPos - nodePosition).normalized;
                Instantiate(boxContainer, nodePosition + dir * distance * 0.5f, Quaternion.identity, transform).transform.right = dir;
                Instantiate(gradientPrefab, previousPos - dir * 0.8f, Quaternion.identity, transform).transform.right = dir;
            }

            GameObject newNode;
            if (i == nodeCount - 1)
                newNode = Instantiate(finishPrefab, nodePosition, Quaternion.identity, transform);
            else
            {
                newNode = Instantiate(nodePrefab, nodePosition, Quaternion.identity, transform);

                if (i > 0)
                {
                    NodeObstacle(nodePosition);
                    Instantiate(coinPrefab, nodePosition, Quaternion.identity, transform);
                }
            }

            previousPos = nodePosition;

            nodes.Add(newNode.transform);
        }

        if (regenerate)
        {
            Clear();
            GenerateLevel();
        }
    }

    void NodeObstacle(Vector3 position)
    {
        if (Random.Range(0, 100) < nodeObstacleProbability)
        {
            int r = Random.Range(0, nodeObsatcles.Length);
            Instantiate(nodeObsatcles[r], position, Quaternion.identity, transform);
        }
    }

    void GenerateObstacle(Vector3 posA, Vector3 posB)
    {
        Vector3 pos = (posB + posA) / 2;
        GameObject obstacle = Instantiate(obstaclePrefabs[Random.Range(0, obstaclePrefabs.Length)], pos, Quaternion.identity, transform);
        obstacle.transform.forward = (posB - posA).normalized;
        obstacles.Add(obstacle.transform);
    }

    Vector3 RandomPointOnCircle(float radius)
    {
        Vector2 pos = Random.insideUnitCircle.normalized * radius;
        return new Vector3(pos.x, 0, pos.y);
    }

    Vector3 RandomPointOnHexagon(Vector3 previusPosition, float distance)
    {
        Vector3 newPos = Vector3.zero;
        int r = Random.Range(-1, 2);
        float degrees = (r * 60f) * Mathf.Deg2Rad;

        newPos = new Vector3(Mathf.Cos(degrees), 0, Mathf.Sin(degrees));
        newPos = previusPosition + newPos.normalized * distance;

        return newPos;
    }

    private void OnDrawGizmos()
    {
        if (nodes != null)
        {
            Gizmos.color = Color.blue;

            for (int i = 0; i < nodes.Count; i++)
            {
                Gizmos.DrawSphere(nodes[i].position, 0.1f);

                if (i > 0)
                {
                    Gizmos.DrawLine(nodes[i - 1].position, nodes[i].position);
                }
            }

            Gizmos.color = Color.red;

            for (int i = 0; i < obstacles.Count; i++)
            {
                Gizmos.DrawSphere(obstacles[i].position, 0.1f);
            }
        }
    }

    public Transform GetNextNode()
    {
        GameManager.Instance.UpdateProgress(lastNodeGiven - 1, nodeCount - 1);

        if (lastNodeGiven < nodeCount)
            lastNodeGiven++;
        else
            GameManager.Instance.GameOver(true);

        return nodes[lastNodeGiven - 1];
    }
}
