﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    [SerializeField] AudioSource sfxSource;
    [SerializeField] AudioSource musicSource;

    [Header("Audio Clip")]
    [SerializeField] AudioClip coinSound;
    [SerializeField] AudioClip winSound;
    [SerializeField] AudioClip lostSound;

    public bool SFX
    {
        get
        {
            return PlayerPrefs.GetInt("SFX", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("SFX", value ? 1 : 0);
            UIManager.Instance.SetSFX(value);
            sfxSource.enabled = value;
        }
    }
    public bool Music
    {
        get
        {
            return PlayerPrefs.GetInt("Music", 1) == 1;
        }
        set
        {
            PlayerPrefs.SetInt("Music", value ? 1 : 0);
            UIManager.Instance.SetMusic(value);
            musicSource.enabled = value;
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        SFX = SFX;
        Music = Music;
    }

    public void Coin()
    {
        sfxSource.PlayOneShot(coinSound);
    }

    public void Win()
    {
        sfxSource.PlayOneShot(winSound);
    }

    public void Lost()
    {
        sfxSource.PlayOneShot(lostSound);
    }

    public void ToggleSFX()
    {
        SFX = !SFX;
    }

    public void ToggleMusic()
    {
        Music = !Music;
    }
}
