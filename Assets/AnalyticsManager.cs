﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsManager : MonoBehaviour
{
    static AnalyticsManager instance = null;

    ConsoleManager console = null;

    public Firebase.FirebaseApp firebase = null;

    uint segmentId = 0;
    uint segmentsAmount = 1;

    public static AnalyticsManager Initialize()
    {
        if (instance == null)
        {
            var obj = new GameObject("AnalyticsManager");
            var cm = obj.AddComponent<AnalyticsManager>();
            DontDestroyOnLoad(obj);
            instance = cm;

            cm.console = GameManager.Instance.console;

            cm.InitializeAnalytics();
        }

        return instance;
    }

    uint CreateSegmentId(uint num)
    {
        segmentsAmount = num;

        return ((uint)SystemInfo.deviceUniqueIdentifier[0]) % segmentsAmount;
    }

    public uint GetSegmentId()
    {
        return segmentId;
    }

    internal void LogEvent(string eventId)
    {
        LogEvent(eventId,"","");
    }

    internal void LogEvent(string eventId, string paramKey, string paramValue)
    {
        if (paramKey == "")
        {
            Amplitude.Instance.logEvent(eventId, null);

            if(firebase!=null) Firebase.Analytics.FirebaseAnalytics.LogEvent(eventId);
        }
        else
        {
            Amplitude.Instance.logEvent(eventId, new Dictionary<string, object>
            {
                { paramKey, paramValue }
            });

            if (firebase != null) Firebase.Analytics.FirebaseAnalytics.LogEvent(eventId, paramKey, paramValue);
        }

        console.Print($"[LogEvent] {eventId} {paramKey} {paramValue}");
    }

    void InitializeAnalytics()
    {
        console.Print("InitializeAnalytics");

        Amplitude amplitude = Amplitude.Instance;
        amplitude.logging = true;
        amplitude.init("7cfe2435021783c7b12958cf810e67e9");


        segmentId = CreateSegmentId(1);
        Amplitude.Instance.setUserProperty("mt_segment", segmentId);

        Debug.Log($"Segment: {segmentId}");
        console.Print($"Segment: {segmentId} Segments amount: {segmentsAmount}");

        console.Print($"[Firebase] Trying to initialize...");

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                firebase = Firebase.FirebaseApp.DefaultInstance;

                // Set a flag here to indicate whether Firebase is ready to use by your app.

                Firebase.Analytics.FirebaseAnalytics.LogEvent("app_launch");

                if (PlayerPrefs.GetInt("LaunchCount", 0) == 0)
                {
                    Firebase.Analytics.FirebaseAnalytics.LogEvent("app_first_launch");
                    PlayerPrefs.SetInt("LaunchCount", 1);
                }


                console.Print($"[Firebase] Initialized: {firebase.Options.ApiKey}");
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.

                console.Print($"[Firebase] Disabled!");
            }
        });


        //--------

        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            FB.Init(() =>
            {
                FB.ActivateApp();

                console.Print("[FB] Activated!");
            });
        }

    }

}
